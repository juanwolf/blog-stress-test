package fr.juanwolf.scenarios

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import scala.concurrent.duration._


/**
 * Created by juanwolf on 16/04/15.
 */
class Scenario extends Simulation {

  val indexPageUrl = "${config.scenario.protocol}://${config.scenario.hostname}"

  object Browse {

    val goToIndex = exec(
      http("Go To Index")
      .get(indexPageUrl)
      .check(status.is(200))
    )

    val goToTag = exec(
      http("Go To Tag")
      .get(indexPageUrl + "/${config.scenario.tag}")
      .check(status.is(200))
    )

    val readArticle = http("Go To Article")
      .get(indexPageUrl + "/${config.scenario.article}")
      .check(status.is(200))

    val readArticleWithoutImage = http("Go To Article Without Image")
      .get(indexPageUrl + "/${config.scenario.articleWithoutImage}")
      .check(status.is(200))

    val goToCategory = http("Go To Category")
      .get(indexPageUrl + "/${config.scenario.category}")
      .check(status.is(200))

  }


  val feeder = jsonFile("config/config.json").circular
  
  val randomer =
    scenario("Randomer")
      .feed(feeder)
      .exec(Browse.goToIndex)
      .pause(5)
      .exec(Browse.readArticle)
      .pause(30)
      .exec(Browse.goToCategory)



  val linkFollower =
    scenario("Link Follower")
      .feed(feeder)
      .exec(Browse.readArticle)
      .pause(30)
      .exec(Browse.goToCategory)


  setUp(
    linkFollower.inject(atOnceUsers(100)),
    randomer.inject(atOnceUsers(100))
  )

}
